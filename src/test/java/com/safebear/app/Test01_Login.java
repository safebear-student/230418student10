package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Test01_Login extends BaseTest {

    @Test
    public void testLogin(){
     //Step 1 test confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());

        // Step 2 click on the Login link and the Login page loads
        welcomePage.clickOnLogin();


        //Step 3 confirm that we're now on the Login page
        assertTrue(loginPage.checkCorrectPage());

        // Step 4 Login with valid credentials
        loginPage.login("testuser","testing");

        //Step 5 Check that  we're nw on the User Page
        assertTrue(userPage.checkCorrectPage());
    }
}